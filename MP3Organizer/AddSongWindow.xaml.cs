﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MP3Organizer.ViewModels;


namespace MP3Organizer
{
    /// <summary>
    /// Interaction logic for AddSongWindow.xaml
    /// </summary>
    public partial class AddSongWindow : Window
    {
        SongViewModel _song;
        MainWindow _mainWindow;

        public AddSongWindow(MainWindow mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;

            _song = new SongViewModel();
            this.DataContext = _song;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            bool validate = Validate();
            if (validate)
            {
                _mainWindow.AddTempSong(_song);
                this.Close();
            }         
            
        }

        private bool Validate()
        {
            if (_song.Title == null)
            {
                MessageBox.Show("Title cannot be empty.");
                return false;
            }
            if (_song.Title.Length > 60)
            {
                MessageBox.Show("Title length must not exceed 60 letters.");
                return false;
            }

            if (_song.Artist == null)
            {
                MessageBox.Show("Artist cannot be empty.");
                return false;
            }
            if (_song.Artist.Length > 40)
            {
                MessageBox.Show("Artist length must not exceed 40 letters.");
                return false;
            }


            if (_song.Album == null)
            {
                MessageBox.Show("Album cannot be empty.");
                return false;
            }
            if (_song.Album.Length > 40)
            {
                MessageBox.Show("Album length must not exceed 40 letters.");
                return false;
            }

            if (_song.Genre == null)
            {
                MessageBox.Show("Genre cannot be empty.");
                return false;
            }
            if (_song.Genre.Length > 20)
            {
                MessageBox.Show("Genre length must not exceed 20 letters.");
                return false;
            }

            return true;
        }
    }
}
