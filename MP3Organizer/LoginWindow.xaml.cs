﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using MP3Organizer.Data.Repository;

namespace MP3Organizer
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private IUsersRepository _usersRepository = new UsersRepository();

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            var user = _usersRepository.GetUserByNameAndPassword(NameTextBox.Text, PassowrdTextBox.Password);
            if (user != null)
            {
                var win = new MainWindow(user);
                win.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong name or password!");                
            }
        }
    }
}
