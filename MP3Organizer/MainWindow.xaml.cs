﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MP3Organizer.Data.Models;
using MP3Organizer.ViewModels;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using MP3Organizer.Data.Repository;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;

namespace MP3Organizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SongListViewModel _songsList;

        public MainWindow(MP3Organizer.Data.Models.User user)
        {
            InitializeComponent();

            // init combobox
            SearchCategoryComboBox.Items.Add("Title");
            SearchCategoryComboBox.Items.Add("Artist");
            SearchCategoryComboBox.Items.Add("Album");
            SearchCategoryComboBox.Items.Add("Genre");
            SearchCategoryComboBox.SelectedValue = "Title";

            Authorize(user);

            LoadData();
        }

        private void Authorize(Data.Models.User user)
        {
            // hide funcionalities from users which are not owners
            if (user is Owner == false)
            {
                AddSong.Visibility = Visibility.Hidden;
                Save.Visibility = Visibility.Hidden;
                Backup.Visibility = Visibility.Hidden;
            }
        }

        public void AddTempSong(SongViewModel song)
        {
            _songsList.Add(song);
        }

        private void LoadData()
        {
            _songsList = new SongListViewModel();
            songsDataGrid.DataContext = _songsList;
        }

        private void AddSong_Click(object sender, RoutedEventArgs e)
        {
            var addSongWindow = new AddSongWindow(this);
            addSongWindow.Show();
        }

        private void Backup_Click(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            var dlg = new Microsoft.Win32.SaveFileDialog();

            //default file name
            dlg.FileName = "backup"
                + DateTime.Now.Year
                + DateTime.Now.Month.ToString("00")
                + DateTime.Now.Day.ToString("00")
                + DateTime.Now.Hour.ToString("00")
                + DateTime.Now.Minute.ToString("00")
                + DateTime.Now.Second.ToString("00");

            dlg.DefaultExt = ".json"; // Default file extension
            dlg.Filter = "JSON files|*.json"; // Filter files by extension 

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                string filename = dlg.FileName;

                var repository = new SongsRepository();
                var songs = repository.Load().ToList();

                string json = JsonConvert.SerializeObject(songs);

                using (var sw = new StreamWriter(filename))
                {
                    sw.Write(json);
                    MessageBox.Show("Database backed up into file: " + filename + ".");
                }
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            _songsList.Save();
            MessageBox.Show("Data saved into Dabatase.");
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            _songsList.Search(SearchInput.Text, SearchCategoryComboBox.SelectedValue.ToString());
        }
    }
}
