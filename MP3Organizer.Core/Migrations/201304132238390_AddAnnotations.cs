namespace MP3Organizer.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Artists", "Name", c => c.String(maxLength: 40));
            AlterColumn("dbo.Songs", "Title", c => c.String(maxLength: 60));
            AlterColumn("dbo.Albums", "Name", c => c.String(maxLength: 40));
            AlterColumn("dbo.Genres", "Name", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Genres", "Name", c => c.String());
            AlterColumn("dbo.Albums", "Name", c => c.String());
            AlterColumn("dbo.Songs", "Title", c => c.String());
            AlterColumn("dbo.Artists", "Name", c => c.String());
        }
    }
}
