﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MP3Organizer.Data.Models;
using MP3Organizer.Data.Repository;
using MP3Organizer.ViewModels;

namespace MP3Organizer
{
    public class SongsRepository : ISongsRepository
    {
        private SongsDbContext _context = new SongsDbContext();

        public void Add(SongViewModel newSong)
        {
            var song = new Song();
            song.Title = newSong.Title;
            song.Length = newSong.Length;

            var albums = _context.Albums.Where(x => x.Name == newSong.Album);
            if (albums.Any())
            {
                song.Album = albums.First();
            }
            else
            {
                var newAlbum = new Album();
                newAlbum.Id = _context.Albums.Any() ? _context.Albums.Max(x => x.Id) + 1 : newAlbum.Id = 1;
                newAlbum.Name = newSong.Album;
                _context.Albums.Add(newAlbum);
                _context.SaveChanges();
                song.Album = newAlbum;
            }

            var artists = _context.Artists.Where(x => x.Name == newSong.Artist);
            if (artists.Any())
            {
                song.Artist = artists.First();
            }
            else
            {
                var newArtist = new Artist();
                newArtist.Id = _context.Artists.Any() ? _context.Artists.Max(x => x.Id) + 1 : newArtist.Id = 1;
                newArtist.Name = newSong.Artist;
                _context.Artists.Add(newArtist);
                _context.SaveChanges();
                song.Artist = newArtist;
            }

            var genres = _context.Genres.Where(x => x.Name == newSong.Genre);
            if (genres.Any())
            {
                song.Genre = genres.First();
            }
            else
            {
                var newGenre = new Genre();
                newGenre.Id = _context.Genres.Any() ? _context.Genres.Max(x => x.Id) + 1 : newGenre.Id = 1;
                newGenre.Name = newSong.Genre;
                _context.Genres.Add(newGenre);
                _context.SaveChanges();
                song.Genre = newGenre;
            }

            _context.Songs.Add(song);
            _context.SaveChanges();
        }

        public void Save(List<SongViewModel> songs)
        {
            foreach (var song in songs)
            {
                Add(song);
            }            
        }

        public List<SongViewModel> Load()
        {
            IQueryable<Song> songs = _context.Songs.Include("Artist").Include("Album").Include("Genre");

            List<SongViewModel> result = SongsToSongViewModelList(songs);
            
            return result;
        }

        private bool Valid(Song song)
        {
            if (song.Album.Name.Length > 40)
                return false;
            if (song.Title.Length > 60)
                return false;
            if (song.Album.Name.Length > 40)
                return false;
            if (song.Genre.Name.Length > 40)
                return false;
            return true;
        }

        private List<SongViewModel> SongsToSongViewModelList(IQueryable<Song> songs)
        {
            var result = new List<SongViewModel>();
            foreach (var song in songs)
            {
                if (Valid(song))
                {
                    var songVM = SongToSongViewModel(song);
                    result.Add(songVM);
                }
                else
                {
                    MessageBox.Show("Song loading error: " + song.Title);
                }
            }
            return result;
        }
        
        private SongViewModel SongToSongViewModel(Song song)
        {
            var songViewModel = new SongViewModel();
            songViewModel.Artist = song.Artist.Name;
            songViewModel.Title = song.Title;
            songViewModel.Length = song.Length;
            songViewModel.Album = song.Album.Name;
            songViewModel.Genre = song.Genre.Name;

            return songViewModel;
        }

    }
}
