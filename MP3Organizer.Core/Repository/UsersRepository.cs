﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MP3Organizer.Data.Models;

namespace MP3Organizer.Data.Repository
{
    public class UsersRepository : IUsersRepository
    {
        public User GetUserByNameAndPassword(string name, string password)
        {
            using (var context = new UsersDbContext())
            {                
                return context.Users.SingleOrDefault(x => x.Name == name && x.Password == password);
            }
        }
    }
}
