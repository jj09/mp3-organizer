﻿using System;
using MP3Organizer.Data.Models;
namespace MP3Organizer.Data.Repository
{
    public interface IUsersRepository
    {
        User GetUserByNameAndPassword(string name, string password);
    }
}
