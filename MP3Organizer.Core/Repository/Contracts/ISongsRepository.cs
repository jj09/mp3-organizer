﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MP3Organizer.ViewModels;
namespace MP3Organizer.Data.Repository
{
    public interface ISongsRepository
    {
        void Add(SongViewModel newSong);
        List<SongViewModel> Load();
        void Save(List<SongViewModel> songs);
    }
}
