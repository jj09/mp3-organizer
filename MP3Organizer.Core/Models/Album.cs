﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP3Organizer.Data.Models
{
    public class Album
    {
        public int Id { get; set; }
        
        [MaxLength(40)]
        public string Name { get; set; }

        public virtual ICollection<Song> Songs { get; set; }
    }
}
