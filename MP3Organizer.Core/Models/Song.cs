﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP3Organizer.Data.Models
{
    public class Song
    {
        public int Id { get; set; }

        [MaxLength(60)]
        public string Title { get; set; }

        public int Length { get; set; }

        public Artist Artist { get; set; }
        public Album Album { get; set; }
        public Genre Genre { get; set; }
    }
}
