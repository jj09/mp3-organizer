﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MP3Organizer.Data.Repository;

namespace MP3Organizer.ViewModels
{
    [Serializable]
    public class SongViewModel
    {
        [MaxLength(40)]
        public string Artist { get; set; }

        [MaxLength(60)]
        public string Title { get; set; }
        
        public int Length { get; set; }

        [MaxLength(40)]
        public string Album { get; set; }

        [MaxLength(20)]
        public string Genre { get; set; }
    }
}
