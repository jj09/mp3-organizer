﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MP3Organizer.ViewModels
{
    public class SongListViewModel
    {
        private readonly SongsRepository _repository = new SongsRepository();

        private List<SongViewModel> DbSongs { get; set; }

        private List<SongViewModel> TempSongs { get; set; }

        public ObservableCollection<SongViewModel> SongsList { get; set; }

        public SongListViewModel()
        {
            DbSongs = _repository.Load();
            SongsList = new ObservableCollection<SongViewModel>(DbSongs);
        }

        public void Search(string input, string searchBy)
        {
            IEnumerable<SongViewModel> songs;
            var allSongs = TempSongs != null ? DbSongs.Union(TempSongs) : DbSongs;
            if (input.CompareTo("") == 0)
            {
                songs = allSongs;
            }
            else
            {
                switch (searchBy)
                {
                    case "Artist":
                        songs = allSongs.Where(x => x.Artist.ToLower() == input.ToLower());
                        break;
                    case "Album":
                        songs = allSongs.Where(x => x.Album.ToLower() == input.ToLower());
                        break;
                    case "Genre":
                        songs = allSongs.Where(x => x.Genre.ToLower() == input.ToLower());
                        break;
                    default:
                        songs = allSongs.Where(x => x.Title.ToLower() == input.ToLower());
                        break;
                }
            }

            SongsList.Clear();
            foreach (var song in songs)
            {
                SongsList.Add(song);
            }
        }

        public void Add(SongViewModel song)
        {
            if (TempSongs == null)
                TempSongs = new List<SongViewModel>();
            TempSongs.Add(song);
            SongsList.Add(song);
        }

        public void Save()
        {
            if (TempSongs != null)
            {
                _repository.Save(TempSongs);
                TempSongs.Clear();
                DbSongs = _repository.Load();
            }
        }
    }
}
